package inf101v22.trafficlight;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import inf101v22.trafficlight.TrafficLight.AlreadyStartedException;

public class TrafficLightTest {
    TrafficLight trafficLight;

    @BeforeEach
    void initializeTrafficLight() {
        this.trafficLight = new TrafficLight();
    }

    @Test
    void testStateAfterInitialization() {
        assertFalse(trafficLight.getRed());
        assertFalse(trafficLight.getYellow());
        assertFalse(trafficLight.getGreen());
    }

    @Test
    void testTurnOn() throws AlreadyStartedException {
        this.trafficLight.turnOn();
        assertTrue(trafficLight.getRed());
    }

    @Test
    void testNextState() throws AlreadyStartedException {
        this.trafficLight.turnOn();
        assertTrue(trafficLight.getRed());
        assertFalse(trafficLight.getGreen());
        assertFalse(trafficLight.getYellow());

        this.trafficLight.goToNextState();
        assertTrue(trafficLight.getRed());
        assertTrue(trafficLight.getYellow());
        assertFalse(trafficLight.getGreen());
    }

    @Test
    void testGoToRed() throws AlreadyStartedException {
        this.trafficLight.turnOn();
        assertTrue(trafficLight.getRed());
        assertFalse(trafficLight.getGreen());
        assertFalse(trafficLight.getYellow());

        this.trafficLight.goToNextState();
        assertTrue(trafficLight.getRed());
        assertTrue(trafficLight.getYellow());
        assertFalse(trafficLight.getGreen());

        this.trafficLight.goToRed();
        assertTrue(trafficLight.getRed());
        assertFalse(trafficLight.getGreen());
        assertFalse(trafficLight.getYellow());
    
    }
}
